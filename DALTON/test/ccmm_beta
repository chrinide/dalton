#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > ccmm_beta.info <<'%EOF%'
   ccmm_beta
   -----------
   Molecule:         H20 in 5 waters 
   Wave Function:    CC2/CCSD / 6-31G  
   Test Purpose:     CCMM for CC2/CCSD 1. hyperpolarizability 
%EOF%

#######################################################################
#  INTEGRAL INPUT
#######################################################################
cat > ccmm_beta.mol <<'%EOF%'
ATOMBASIS
QM/MM H2O(QM)+ 5 H2O(MM)
------------------------
    4    0         1 1.00D-12
        8.0   1    Bas=6-31G 
O            0.000000        0.000000        0.000000 0 1
        1.0   2    Bas=6-31G 
H           -0.756799        0.000000        0.586007 0 2
H            0.756799        0.000000        0.586007 0 3
   -0.669     5    Bas=MM
O           -6.022295       -6.249876       -2.389355 1 1
O           -0.590747        4.825666       -1.709744 2 1
O            2.365069       -0.266593        1.169946 3 1
O           -8.979615       -1.935917       -5.707554 4 1
O           -5.696915       -2.203270        0.274131 5 1
    0.3345   10    Bas=MM
H           -6.934736       -6.264225       -2.100595 1 2
H           -5.562128       -6.801680       -1.756974 1 3
H           -1.493716        5.142033       -1.683118 2 2
H           -0.065506        5.576575       -1.433333 2 3
H            2.261479       -1.103382        1.622925 3 2
H            3.132239       -0.390013        0.611059 3 3
H           -8.312439       -2.243695       -5.094117 4 2
H           -8.719849       -2.307644       -6.550450 4 3
H           -4.847884       -2.109424       -0.157695 5 2
H           -6.336813       -2.008951       -0.410639 5 3
%EOF%
#######################################################################
#  QM/MM INTERACTION INPUT
#######################################################################
cat > ccmm_beta.pot <<'%EOF%'
**SYSTP
.NUMMTP
 1
.TYPE
 0
.MODEL
 SPC_E01
.CHARGS (It is important to give the charges in the same order as the coordinates)
 3
 -0.669
 0.3345
 0.3345
.ALPISO
 1
 9.718
*******
.TYPE
 1-5
.MODEL
 SPC_E01  # This model includes the polarization in the optimization of
.ALPISO   # the wave function in a mean field approximation. This intro-
 1        # duces a coupling of the t and t-bar equations and the model
 9.718    # is therefore more expensive than a vacuum calculation of the
*******   # same molecule. See *CCSLV, .MXINIT in DALTON.INP!!!
**TWOIA (i,j=0,1,2,...,N; if i=0 then j.neq.0)
.LJ_A
 2
 2083000
 2083000
.LJ_B
 2
 45.21
 45.21
**END OF
%EOF%
#
#######################################################################
#  DALTON INPUT
#######################################################################
cat > ccmm_beta.dal <<'%EOF%'
**DALTON INPUT
.RUN WAVE FUNCTION
*QM3
.QM3
.THRDIP
 1.0D-12
.MAXDIP
 80
!.OLDTG
**INTEGRALS
.DIPLEN
.NUCPOT
.NELFLD
.THETA
.SECMOM
**WAVE FUNCTIONS
.CC
*SCF INP
.THRESH
1.0D-12
*CC INP
.CC2
.CCSD
.THRLEQ
 1.0D-12
.THRENR
 1.0D-12
.MAX IT
 90
.MXLRV
 680
*CCSLV
.CCMM
.ETOLSL
 1.0D-11
.TTOLSL
 1.0D-11
.LTOLSL
 1.0D-11
.MXSLIT
 200
.MXINIT
 4 5
*CCFOP
.DIPMOM
.NONREL
*CCQR
.DIPOLE
.SHGFRE
 1
 0.0040
**END OF
%EOF%
#
#######################################################################
#  CHECK SCRIPT
#######################################################################
#
echo $CHECK_SHELL >ccmm_beta.check
cat >>ccmm_beta.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

CRIT1=`$GREP " CC2 Total  *energy: *\-76.12082433.." $log | wc -l`
CRIT2=`$GREP "CCSD Total  *energy: *\-76.1264845[12].." $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2 ` 
CTRL[1]=2
ERROR[1]="ENERGIES NOT CORRECT"

#
# Test of CC2 hyperpolarizabilities
#
CRIT1=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * 2\.844499." $log | wc -l`
CRIT2=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -25\.44827." $log | wc -l`
CRIT3=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -25\.44827." $log | wc -l`
CRIT4=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -25\.44950." $log | wc -l`
CRIT5=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -18\.32522." $log | wc -l`
TEST[2]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 `
CTRL[2]=5
ERROR[2]="CC2 FIRST HYPERPOLARIZABILITY NOT CORRECT"

#
# Test of CCSD hyperpolarizabilities
#
CRIT1=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * 2\.663147." $log | wc -l`
CRIT2=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -23\.69945." $log | wc -l`
CRIT3=`$GREP "XDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -23\.69945." $log | wc -l`
CRIT4=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * XDIPLEN * \(unrel\.\) * ( |0)\.0040 * -23\.70054." $log | wc -l`
CRIT5=`$GREP "ZDIPLEN * \(unrel\.\) * ( \-|\-0)\.0080 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * ZDIPLEN * \(unrel\.\) * ( |0)\.0040 * -17\.39399." $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4 \+ $CRIT5 `
CTRL[3]=5
ERROR[3]="CCSD FIRST HYPERPOLARIZABILITY NOT CORRECT"

PASSED=1 
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi                
%EOF%
chmod +x ccmm_beta.check
#######################################################################
